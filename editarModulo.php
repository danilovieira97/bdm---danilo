<?php
session_start();
include_once("funcaoBanco.php");
$idmodulo = $_GET['idmodulo'];
if (!isset($_SESSION['nome'])) {
  header("location: index.php?msg=Favor realizar seu login!");
}
$sql = "SELECT * from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='bancomodulo'";
$res = executaSQL($sql);
?>
<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Edição- de Módulo</title>
  <meta charset="utf-8">
  <style>
    body {
      background-image: url("loja2.png");
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
    }

    div {
      background-color: whitesmoke;
      border: 20px black;
      padding: 15px;
      border-radius: 10px;
      width: 500px;
      margin-left: auto;
      margin-right: auto;
      margin-top: 200px;
    }

    p {
      color: black;
    }

    button {
      border: black;
      border-radius: 10px;
    }

    input {
      border-radius: 10px;
    }
  </style>
</head>

<body>
  <center>
    <div>
      <form action="bdmodulo.php" method="post">
        <img src="logo-assinatura.png">
        <br></br>
        <?php
           foreach ($res as $indice => $bancomodulo) {
             if ($indice == 0){
              echo "<input type='hidden' name='idmodulo' value='$idmodulo'>";
             } else{
          echo "$bancomodulo[COLUMN_NAME] : <input type='text' class='remover' id='$idmodulo' name=$bancomodulo[COLUMN_NAME]> <br><br> "; 
           }
          }
        ?>
        <p><button type="submit">Atualizar Módulo</button></p>
      </form>
      <form action="delModulo.php" method="post">
        <input type="hidden" name="idmodulo" value="<?php echo $idmodulo; ?>">
        <p><input type="submit" value="Deletar Módulo"></p>
        <p><a href="menu.php">Voltar ao Menu</a></p>
      </form>
    </div>
  </center>
</body>

</html>