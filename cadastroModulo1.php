<?php
session_start();
$idmodulo = '0';
if (!isset($_SESSION['nome'])) {
  header("location: index.php?msg=Favor realizar o Login!");
}
?>
<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Cadastro de Módulo</title>
  <meta charset="utf-8">
  <style>
    body {
      background-image: url("loja2.png");
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;
    }

    div {
      background-color: whitesmoke;
      border: 20px black;
      padding: 15px;
      border-radius: 10px;
      width: 500px;
      margin-left: auto;
      margin-right: auto;
      margin-top: 200px;
    }

    p {
      color: black;
    }

    button {
      border: black;
      border-radius: 10px;
    }

    input {
      border-radius: 10px;
    }
  </style>
</head>

<body>
  <center>
    <div>
      <form action="bdmodulo.php" method="post">
        <img src="logo-assinatura.png">
        <input type="hidden" name="idmodulo" value="<?php echo $idmodulo; ?>">
        <p><br>Nome do Módulo *: <input type="text" name="nomemodulo"></p>
        <p>Descricao: <input type="text" name="descricao"> </p>
        <p>Categoria: <input type="text" name="categoria"> </p>
        <p>Link *: <input type="text" name="link"> </p>
        <p>Composer *: <input type="text" name="composer"> </p>
        <p>Git *: <input type="text" name="git"> </p>
        <p>Campos com * obrigatórios</p>
        <button type="submit">Cadastrar</button>
        <p><a href="formColuna.php">Criação de nova Coluna</a></p>
        <p><a href="cadastroCsv.php">Cadastrar por CSV</a></p>
        <p><a href="menu.php">Voltar ao Menu</a></p>
      </form>
    </div>
  </center>
</body>

</html>