<?php
session_start();
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar o Login!");
}
include_once("funcaoBanco.php");
$pesquisar = $_POST['pesquisar'];
$selectPesquisa = $_POST['selectPesquisa'];
$sql = "SELECT * FROM bancomodulo WHERE $selectPesquisa LIKE '%$pesquisar%' ORDER BY nomemodulo";
$res = executaSQL($sql);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Pesquisar Modulos</title>
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 900px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 150px;

        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }

        table,
        td,
        th,
        tfoot {
            color: white;
            border: solid 2px #000;
            padding: 5px;
            border-radius: 10px;
        }

        th {
            background-color: rgb(153, 0, 255);
        }

        #rodape {
            background-color: #999;
        }

        #botao {
            color: white;
            background-color: #999;
        }

        #botao1 {
            color: white;
            background-color: rgb(153, 0, 255);
        }

        caption {
            font-size: x-large;
        }

        colgroup {
            background: #F60;
        }

        .coluna1 {
            background: #F66;
        }

        .coluna2 {
            background: #F33;
        }

        .coluna3 {
            background: #F99;
        }

        #linhas {
            color: black;
            background-color: rgb(217, 210, 233);
        }

        #linha1 {
            color: black;
            background-color: rgb(180, 167, 214);
        }
    </style>
</head>

<body>
    <div style="overflow-y:scroll; height:600px">
        <center><img src="logo-assinatura.png"></center>
        <table>
            <thead>
                <tr>
                    <th colspan="4">Pesquisa Módulos</th>
                    <form action="csvModulo.php" method="post">
                        <input type="hidden" name="pesquisa" value="<?php echo $sql ?>">
                        <th colspan="1" id="botao"><input type="submit" value="Gerar Arquivo" id="botao1"></input></th>
                    </form>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="5" id="rodape"><?php echo "<center><a href='menu.php'>Voltar ao Menu</center></a>"; ?></td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td id="linha1">ID Módulo</td>
                    <td id="linha1">Nome Módulo</td>
                    <td id="linha1">Categoria</td>
                    <td id="linha1">Link</td>
                    <?php if ($_SESSION['permissao'] == '1' || $_SESSION['permissao'] == '2') {
                        echo "<td id='linha1'>Editar</td>";
                    } ?>
                </tr>
                <center>
                    <?php foreach ($res as $indice => $bancomodulo) {
                        echo '<tr>';
                        echo '<td id="linhas">' . $bancomodulo['idmodulo'] . '</td>';
                        echo '<td id="linhas">' . $bancomodulo['nomemodulo'] . '</td>';
                        echo '<td id="linhas">' . $bancomodulo['categoria'] . '</td>';
                        echo '<td id="linhas"><a href=' . $bancomodulo['link'] . '>' . $bancomodulo['link'] . '</a></td>';
                        if ($_SESSION['permissao'] == '1' || $_SESSION['permissao'] == '2') {
                            echo '<td id="linhas"><a href=editarModulo.php?idmodulo=' . $bancomodulo['idmodulo'] . '>Editar Módulo</a></td>';
                        }
                        echo '</tr>';
                    } ?>
                </center>
            </tbody>
        </table>
    </div>
</body>

</html>