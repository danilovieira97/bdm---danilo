<?php
include_once("funcaoBanco.php");
$pesquisa = $_POST['pesquisa'];
// output headers so that the file is downloaded rather than displayed
header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=usuario.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
fputcsv($output, array('ID Usuário', 'Nome Usuário', 'E-mail', 'Senha', 'Permissão'));

// fetch the data
$con  = mysqli_connect('127.0.0.1', 'root', 'magento', 'BDM');
$rows = mysqli_query($con, $pesquisa);

// loop over the rows, outputting them
while ($row = mysqli_fetch_assoc($rows)) fputcsv($output, $row);
?>