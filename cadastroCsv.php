<?php
session_start();
if (!isset($_SESSION['nome'])) {
  header("location: index.php?msg=Favor realizar o Login!");
  }

if (!$_SESSION['permissao'] == 1){
    echo "Você não tem permissão para acessar essa área";
    echo "<p><a href='menu.php'>Voltar ao Menu</a></p>";
}
?>
<!DOCTYPE html>
<html>
  <head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cadastro de Módulo CSV</title>
    <meta charset="utf-8">
    <style>
        body{
          background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        div{
            background-color:whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 500px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 200px;
        }
        p{
            color: black;
        }
        button{
            border: black;
            border-radius: 10px;
        }
        input{
            border-radius: 10px;
        }
    </style>
  </head>
  <body>
    <center>
    <div>
    <img src="logo-assinatura.png"><br><br>
      <form action="processa.php" method="post" enctype="multipart/form-data">
        <label>Arquivo.csv</label>
        <input type="file" name="file">
        <input type='submit' value="Enviar Arquivo">
        <p><a href='menu.php'><br>Voltar ao Menu</a></p>
        
    </form>
    </div>
    </<img>
    </body>
    </html>