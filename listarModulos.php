<?php
session_start();
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar o Login!");
}
header("Content-type: text/html; charset=utf-8");
include_once("limitarTexto.php");
include_once("funcaoBanco.php");
$sql = "SELECT * FROM bancomodulo ORDER BY nomemodulo";

$res = executaSQL($sql);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Listar Modulos Cadastrados</title>
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 1100px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 150px;

        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }

        table,
        td,
        th,
        tfoot {
            color: white;
            border: solid 2px #000;
            padding: 5px;
            border-radius: 10px;
        }

        th {
            background-color: rgb(153, 0, 255);
        }

        #rodape {
            background-color: #999;
        }

        #botao {
            background-color: green;
        }

        caption {
            font-size: x-large;
        }

        colgroup {
            background: #F60;
        }

        .coluna1 {
            background: #F66;
        }

        .coluna2 {
            background: #F33;
        }

        .coluna3 {
            background: #F99;
        }
        #linhas{
            color: black;
            background-color: rgb(217,210, 233);
        }
        #linha1{
            color: black;
            background-color: rgb(180, 167, 214);
        }
    </style>
    </style>
</head>

<body>
    <div style="overflow-y:scroll; height:600px">
        <center><img src="logo-assinatura.png"></center>
        <table>
            <thead>
                <tr>
                    <th colspan="2">Listar Módulos</th>

                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="2" id="rodape"><?php echo "<center><a href='menu.php'>Voltar ao Menu</center></a>"; ?></td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td id="linha1"><b>Nome Módulo</b></td>
                    <td id="linha1"><b>Descrição</b></td>
                </tr>
                <center>
                    <?php foreach ($res as $indice => $bancomodulo) {
                        echo '<tr>';
                        echo '<td id="linhas">' . $bancomodulo['nomemodulo'] . '</td>';
                        echo '<td id="linhas">' . $bancomodulo['descricao'] . '</td>';
                    } ?>
                </center>
            </tbody>
        </table>
        </table>
    </div>
</body>

</html>