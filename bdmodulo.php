<?php

session_start();
include_once("funcaoBanco.php");
if (!isset($_SESSION['nome'])) {
	header("location: index.php?msg=Favor realizar o Login!");
}

 foreach ($_POST as $key => $value) {
 $indice[$key] = $value ;
 }
 unset($indice['idmodulo']);

 $idmodulo = $_POST['idmodulo'];
 $nomemodulo = $_POST['nomemodulo'];
 $descricao = $_POST['descricao'];
 $categoria = $_POST['categoria'];
 $link = $_POST['link'];
 $composer = $_POST['composer'];
 $git = $_POST['gitlab'];
?>

<!DOCTYPE html>
<html>

<head>
	<style>
		body {
			background-image: url("loja2.png");
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
		}

		div {
			background-color: whitesmoke;
			border: 20px black;
			padding: 15px;
			border-radius: 10px;
			width: 360px;
			margin-left: auto;
			margin-right: auto;
			margin-top: 250px;

		}

		p {
			color: black;
		}

		button {
			border: black;
			border-radius: 10px;
		}

		input {
			border-radius: 10px;
		}
	</style>
</head>

<body>
	<div>
		<center>
			<?php
			if ($_SESSION['permissao'] == '1' || $_SESSION['permissao'] == '2') {

				$keys = '';
				$count = 0;
				
				foreach ($indice as $key =>$value) { 
					$count += 1;
					$keys .= $key;
					if(count($indice) > $count){
						$keys .= " ,";
					}else{
						$keys .= "";
					}
				}						
				$values = '';
				$count = 0;
				foreach ($indice as $value) { 
					$count += 1;
					
					if (is_string($value)){
						$values .= "'".$value."'";
					} else{
						$values .= $value;
					}
					if(count($indice) > $count){
						$values .= " ,";
					}else{
						$values .= "";
					}
				}
				$update = '';
 				$count = 0;
				foreach($indice as $keys => $values){
				$count+= 1;
				$update .= $keys .'='. "'$values'" ;
				if(count($indice) > $count){
					$update .= ", ";
				}else{
					$update .= "";
				}
			}
				
				if ($nomemodulo != '' && $link != '' && $composer != '' && $git != '') {
					if ($idmodulo == '0') {
						$sql = "INSERT into bancomodulo ($keys) VALUES ($values)";
						$res = executaSQL($sql);
						$acao = "cadastrado";
					} else {
						$sql = "UPDATE bancomodulo SET $update WHERE idmodulo=$idmodulo";
						$res = executaSQL($sql);
						$acao = "atualizado";
					}

					if ($res) {
						echo "Módulo $nomemodulo $acao com sucesso!<br>";
						echo "<a href='menu.php'>Voltar ao Menu</a>";
					}
				} else {
					echo "Campos obrigatórios não foram prenchidos corretamente<br>";
					echo "<a href='cadastroModulo.php'>Voltar ao Cadastro de Modulos</a>";
				}
			} else {
				echo "Você não tem permissão para acessar essa página";
				echo "<p><a href='menu.php'>Voltar ao Menu</a></p>";
			}
			?>
		</center>
	</div>
</body>

</html>