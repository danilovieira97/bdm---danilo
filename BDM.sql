-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Nov 22, 2019 at 10:30 AM
-- Server version: 10.4.10-MariaDB-1:10.4.10+maria~bionic
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `BDM`
--

-- --------------------------------------------------------

--
-- Table structure for table `bancomodulo`
--

CREATE TABLE `bancomodulo` (
  `idmodulo` int(100) NOT NULL,
  `nomemodulo` varchar(255) NOT NULL,
  `descricao` text DEFAULT NULL,
  `categoria` varchar(255) DEFAULT NULL,
  `link` varchar(255) NOT NULL,
  `composer` varchar(20) NOT NULL,
  `gitlab` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bancomodulo`
--

INSERT INTO `bancomodulo` (`idmodulo`, `nomemodulo`, `descricao`, `categoria`, `link`, `composer`, `gitlab`) VALUES
(1, 'Embalagens Produto', 'O mÃ³dulo tem como objetivo permitir ao lojista cadastrar embalagens diferentes para seus produtos, que devem ser selecionadas pelo cliente ao adicionar o produto ao carrinho. Obviamente essa possibilidade sÃ³ existe se no produto o lojista tiver cadastrado mais de 1 opÃ§Ã£o de embalagem. A embalagem selecionada pelo cliente serÃ¡ exibidas no carrinho e tambÃ©m no order.', 'CatÃ¡logo', 'https://gitlab.com/bis2bis-modules/modulo-embalagens-produto', 'No', 'Gitlab'),
(2, 'Engine', '\"MÃ³dulo que cria a capacidade de agrupar todos os arquivos dos outros mÃ³dulos em uma sÃ³ pasta. Isso cria a capacidade de instalaÃ§Ã£o de mÃ³dulos via Composer.\"', 'Desenvolvimento', 'https://gitlab.com/bis2bis-modules/engine', 'No', 'Gitlab'),
(3, 'Smart Hint', '', 'IntegraÃ§Ã£o', 'https://gitlab.com/bis2bis-modules/-smart-hint', 'No', 'Gitlab'),
(4, 'Checkout Grid ', 'O mÃ³dulo tem como objetivo exibir os produtos divididos por grid no minicart, cart e osc.', 'CatÃ¡logo', 'https://gitlab.com/bis2bis-modules/-checkout-grid', 'No', 'Gitlab');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(3) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `permissao` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nome`, `email`, `senha`, `permissao`) VALUES
(1, 'Danilo', 'danilo@hotmail.com', 'e7a4293b64e0b9c82d97be1d4d25c34c', 1),
(2, 'Vitor', 'vitor@hotmail.com', 'b17f831f6881a76161a5c06a94473a6b', 1),
(3, 'Marcao', 'marcao@hotmail.com', 'b2a286686ff050566238453fdfbcaf47', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bancomodulo`
--
ALTER TABLE `bancomodulo`
  ADD PRIMARY KEY (`idmodulo`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bancomodulo`
--
ALTER TABLE `bancomodulo`
  MODIFY `idmodulo` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
