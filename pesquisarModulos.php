<?php

session_start();
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar o Login!");
}

include_once("funcaoBanco.php");
?>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
<title>Pesquisar Modulos</title>
<style>
    body {
        background-image: url("loja2.png");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    div {
        background-color: whitesmoke;
        border: 20px black;
        padding: 15px;
        border-radius: 10px;
        width: 400px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 250px;

    }

    p {
        color: black;
    }

    button {
        border: black;
        border-radius: 10px;
    }

    input {
        border-radius: 10px;
    }

    body {
        margin: 50px auto;
    }

    p {
        margin: 20px
    }

    select.basic {
        width: 200px;
        height: 35px;
    }

    select.simple {
        background: #f5f5f5;
        border: 0;
    }
</style>

<head>
</head>

<body>
    <div>
        <center><img src="logo-assinatura.png"></center>
        <form action="pesquisar.php" method="POST">
            Pesquisar: <input type="text" name="pesquisar"> <input type="submit" value="PESQUISAR">
            <center>
                <p><select name="selectPesquisa" class="basic">
                        <option value="nomemodulo">Nome</option>
                        <option value="descricao">Descrição</option>
                        <option value="categoria">Categoria</option>
                        <option value="link">Link</option>
                    </select></p>
        </form>
        <p><a href="menu.php">Voltar ao Menu</a></p>
        </center>
    </div>
</body>

</html>