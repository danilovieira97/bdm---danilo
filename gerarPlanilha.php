<?php
session_start();
$pesquisa = $_POST['pesquisa'];
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar o Login!");
}
include_once("funcaoBanco.php");
$sql = $pesquisa;
$res = executaSQL($sql);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Download Planilha</title>
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 600px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 250px;

        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        #botaoexcel {}

        input {
            border-radius: 10px;
        }

        table,
        td,
        th,
        tfoot {
            border: solid 1px #000;
            padding: 5px;
            border-radius: 5px;
        }

        th {
            background-color: #999;
        }

        #rodape {
            background-color: #999;
        }

        #botao {
            background-color: green;
        }

        caption {
            font-size: x-large;
        }

        colgroup {
            background: #F60;
        }

        .coluna1 {
            background: #F66;
        }

        .coluna2 {
            background: #F33;
        }

        .coluna3 {
            background: #F99;
        }
    </style>
</head>

<body>
    <div>
        <center>
            <h1>Sua Planilha foi gerada com sucesso</h1>
            <p><a href="menu.php">Voltar ao Menu</a></p>
        </center>
    </div>
</body>

</html>