<?php
session_start();
include_once("funcaoBanco.php");
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar o Login!");
}
$nomecampo = $_POST['nomecampo'];
$tipodado = $_POST['tipoDado'];
$null = $_POST['null'];
$tamanho = $_POST['tamanho'];
if (isset($null)) {
    $null = "NULL";
} else {
    $null = "NOT NULL";
}
?>

<!DOCTYPE html>
<html>

<head>
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 360px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 250px;

        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }
    </style>
</head>

<body>
    <div>
        <center>
            <?php
            if ($_SESSION['permissao'] == '1') {
                $sql = "ALTER TABLE bancomodulo ADD $nomecampo $tipodado($tamanho) $null ";
                $res = executaSQL($sql);
            }
            if ($res) {
                echo "Coluna '$nomecampo' adicionada com sucesso";
                echo "<br><a href='menu.php'>Voltar ao Menu</a>";
            } else {
                echo "Erro ao adicionar a coluna";
                echo "<br><a href='menu.php'>Voltar ao Menu</a>";
            }
            ?>
        </center>
    </div>
</body>

</html>