 <?php
    include_once("funcaoBanco.php");
    $arquivo = $_FILES["file"]["tmp_name"];
    $nome = $_FILES["file"]["name"];

    error_reporting(0);
    ini_set(“display_errors”, 0);
    ?>
 <!DOCTYPE html>
 <html>

 <head>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <title>Cadastro de Módulo CSV</title>
     <meta charset="utf-8">
     <style>
         body {
             background-image: url("loja2.png");
             background-position: center;
             background-repeat: no-repeat;
             background-size: cover;
         }

         div {
             background-color: whitesmoke;
             border: 20px black;
             padding: 15px;
             border-radius: 10px;
             width: 500px;
             margin-left: auto;
             margin-right: auto;
             margin-top: 300px;
         }

         p {
             color: black;
         }

         button {
             border: black;
             border-radius: 10px;
         }

         input {
             border-radius: 10px;
         }
     </style>
 </head>

 <body>
     <center>
         <div>
             <?php
                $ext = explode(".", $nome);
                $extensao = end($ext);

                if ($extensao != "csv") {
                    echo "<h3>Extensão Inválida</h3>";
                    echo "<p><a href='cadastroCsv.php'>Voltar ao Cadastro por CSV</a></p>";
                        echo "<p><a href='menu.php'>Voltar ao Menu</a></p>";
                } else {
                    $objeto = fopen($arquivo, 'r');
                    while (($dado = fgetcsv($objeto, 10000, ',')) !== FALSE) {
                        $sql = "INSERT INTO bancomodulo (nomemodulo, descricao, categoria, link, composer, gitlab) values ( '$dado[0]','$dado[1]', '$dado[2]','$dado[3]', '$dado[4]', '$dado[5]')";
                        $res = executaSQL($sql);
                    }
                    if ($res) {
                        echo "<h3>Dados inseridos com sucesso</h3>";
                        echo "<p><a href='cadastroCsv.php'>Voltar ao Cadastro por CSV</a></p>";
                        echo "<p><a href='menu.php'>Voltar ao Menu</a></p>";
                    } else {
                        echo "<h3>Erro ao inserir os dados</h3>";
                        echo "<p><a href='cadastroCsv.php'>Voltar ao Cadastro por CSV</a></p>";
                        echo "<p><a href='menu.php'>Voltar ao Menu</a></p>";
                    }
                }
                ?>

         </div>
     </center>
 </body>

 </html>