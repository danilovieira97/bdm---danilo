<?php
session_start();
include_once("funcaoBanco.php");
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar seu login!");
}
$idusuario = $_POST['idusuario'];
?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Deletar Usuário</title>
    <meta charset="utf-8">
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 500px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 200px;
        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }
    </style>
</head>

<body>

    <body>
        <center>
            <div>
                <p><img src="logo-assinatura.png"></p>
                <?php 
                    $sql = "DELETE from usuario where idusuario='$idusuario'";
                    $res = executaSQL($sql);
                    if ($res) {
                        echo "<p>Usuário excluído com sucesso!</p>";
                    } else {
                        echo "<p>Usuário não pode ser excluído!</p>";
                    }
            

                echo "<a href='menu.php'>Voltar ao Menu</a>";
                ?>
            </div>
        </center>
    </body>

</html>