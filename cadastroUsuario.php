<?php
session_start();
if (!isset($_SESSION['nome'])) {
  header("location: index.php?msg=Favor realizar o Login!");
  }
?>
<!DOCTYPE html>
<html>
  <head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cadastro de Usuário</title>
    <meta charset="utf-8">
    <style>
        body{
          background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        div{
            background-color:whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 480px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 200px;
        }
        p{
            color: black;
        }
        button{
            border: black;
            border-radius: 10px;
        }
        input{
            border-radius: 10px;
        }
        body {
            margin: 50px auto;
            }
        p {margin: 20px}

        select.basic {
            width: 200px;
            height: 35px;
            }
         select.simple {
            background: #f5f5f5;
            border: 0;
        }
    </style>
  </head>
  <body>
    <center>
    <div>
      <form action="bdusuario.php" method="post">
          <img src="logo-assinatura.png">
        <input type="hidden" name="idusuario" value="0">
        <p><br>Nome *: <input type="text" name="nome"></p>
        <p>E-mail *: <input type="email" name="email"> </p>
        <p>Senha *: <input type="password" name="senha"> </p>
        <p>Confirmar Senha *: <input type="password" name="confirmarSenha"> </p>
        <p>Campos com * obrigatórios</p>
        <p>Selecione a Permissão *:</p>
        <p><select name="selectPermissao" class="basic">
        <option value="1" >Total</option>
        <option value="2" >Edição</option>
        <option value="3">Visualização</option>
    </select></p>
        <button type="submit">Cadastrar</button>
        <p><a href="menu.php">Voltar ao Menu</a></p>
      </form>
    </div>
  </center>
  </body>
</html>