<?php
session_start();
include_once("funcaoBanco.php");
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar seu login!");
}
$sql = "SELECT * FROM usuario";
$res = executaSQL($sql);
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Listar Usuários</title>
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 675px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 175px;

        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }

        table,
        td,
        th,
        tfoot {
            color: white;
            border: solid 2px #000;
            padding: 5px;
            border-radius: 10px;
        }

        th {
            background-color: rgb(153, 0, 255);
        }

        #rodape {
            background-color: #999;
        }

        #botao {
            background-color: green;
        }

        caption {
            font-size: x-large;
        }

        colgroup {
            background: #F60;
        }

        .coluna1 {
            background: #F66;
        }

        .coluna2 {
            background: #F33;
        }

        .coluna3 {
            background: #F99;
        }

        #linhas {
            color: black;
            background-color: rgb(217, 210, 233);
        }

        #linha1 {
            color: black;
            background-color: rgb(180, 167, 214);
        }
        #botao {
            color: white;
            background-color: #999;
        }
        #botao1 {
            color: white;
            background-color: rgb(153, 0, 255);
        }
    </style>
</head>

<body>
    <div style="overflow-y:scroll; height:450px">
        <center><img src="logo-assinatura.png">
            <table>
                <thead>
                    <tr>
                        <th colspan="4">Listar Usuários</th>
                        <form action="csvUsuario.php" method="post">
                        <input type="hidden" name="pesquisa" value="<?php echo $sql ?>">
                        <th colspan="1" id="botao"><input type="submit" value="Gerar Arquivo" id="botao1"></input></th>
                    </form>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="5" id="rodape"><?php echo "<center><a href='menu.php'>Voltar ao Menu</center></a>"; ?></td>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td id="linha1">ID Usuário</td>
                        <td id="linha1">Nome</td>
                        <td id="linha1">E-mail</td>
                        <td id="linha1">Permissão</td>
                        <td id="linha1">Editar</td>
                    </tr>
                    <?php foreach ($res as $indice => $usuario) {
                        if ($usuario['permissao'] == '1') {
                            $usuario['permissao'] = 'Total';
                        } elseif ($usuario['permissao'] == '2') {
                            $usuario['permissao'] = 'Edição';
                        } else {
                            $usuario['permissao'] = 'Visualização';
                        }
                        echo '<tr>';
                        echo '<td id="linhas">' . $usuario['idusuario'] . '</td>';
                        echo '<td id="linhas">' . $usuario['nome'] . '</td>';
                        echo '<td id="linhas">' . $usuario['email'] . '</td>';
                        echo '<td id="linhas">' . $usuario['permissao'] . '</td>';
                        echo '<td id="linhas"><a href=editarUsuario.php?idusuario=' . $usuario['idusuario'] . '>Editar Usuário</a></td>';
                        echo '</tr>';
                    } ?>
                </tbody>
            </table>
        </center>
    </div>
</body>

</html>