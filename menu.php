<?php
session_start();
$nome = $_SESSION['nome'];
if (!isset($_SESSION['nome'])) {
  header("location: index.php?msg=Favor realizar o Login!");
  }
?>

<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <title>Menu BDM</title>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body{
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        div{
                background-color:whitesmoke;
                border: 20px black;
				padding: 15px;
				border-radius: 10px;
				width: 400px;
				margin-left: auto;
				margin-right: auto;
                margin-top: 250px;
            
        }
        p{
            color: black;
        }
        button{
            border: black;
            border-radius: 10px;
        }
        input{
            border-radius: 10px;
        }
    </style>
</head>
<body>   
</body>
        <center>
            <div>
                <h2>Bem Vindo <?php echo "$nome" ?></h2><br>
                <p><a href="listarModulos.php">Listar Módulos</a></p>
                <p><a href="pesquisarModulos.php">Pesquisar Módulos</a></p>
                <?php if($_SESSION['permissao'] == '1'){
                echo "<p><a href='cadastroModulo.php'>Cadastrar Módulos</a></p>";
                echo "<p><a href='cadastroUsuario.php'>Cadastrar Usuário</a></p>";
                echo "<p><a href='listarUsuario.php'>Listar Usuários</a></p>";
                }
                if($_SESSION['permissao'] == '2'){
                    echo "<p><a href='cadastroModulo.php'>Cadastrar Módulos</a></p>";
                }
                ?>
                <p><a href="logoff.php">Sair do Sistema</a></p>
                
            </div>
        </center>

</html>