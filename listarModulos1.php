<?php
	session_start();
	$con = include_once("funcaoBanco.php");
 ?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Listar Módulos</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<style>
		body{
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
		div{
                background-color:whitesmoke;
				border-radius: 10px;
        }
		</style>
	<head>
	<body>
		<?php
			//Verificar se esta sendo passado na URL a página atual, senão é atribuido a pagina
			$pagina=(isset($_GET['pagina'])) ? $_GET['pagina'] : 1;
			
			//Selecionar todos os itens da tabela 
			$res = "SELECT * FROM bancomodulo";
			$res = mysqli_query($con , $res);
			
			//Contar o total de itens
			$total_msg_contatos = mysqli_num_rows($res);
			
			//Seta a quantidade de itens por página
			$quantidade_pg = 20;
			
			//calcular o número de páginas 
			$num_pagina = ceil($total_msg_contatos/$quantidade_pg);
			
			//calcular o inicio da visualizao	
			$inicio = ($quantidade_pg*$pagina)-$quantidade_pg;
			
			//Selecionar  os itens da página
			$res = "SELECT * FROM bancomodulo limit $inicio, $quantidade_pg";
			$res = mysqli_query($con , $res);
			$total_msg_contatos = mysqli_num_rows($res);
			
		?>
		<div class="container theme-showcase" role="main">
			<div class="page-header">
				<h1>Lista de Módulos Cadastrados</h1> 
			</div>
			<img src="logo-assinatura.png">
			<div class="row espaco">
				<div class="pull-right">					
					<a href="form_contato.php"><button type='button' class='btn btn-sm btn-success'>Cadastrar</button></a>
					<a href="gerar_planilha.php"><button type='button' class='btn btn-sm btn-success'>Gerar Excel</button></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12"> 
					<table class="table">
						<thead>
							<tr>
								<th class="text-center">ID Módulo</th>
								<th class="text-center">Nome Módulo</th>
								<th class="text-center">Descrição</th>
								<th class="text-center">Categoria</th>
								<th class="text-center">Inserido</th>
								<th class="text-center">Ação</th>
							</tr>
						</thead>
						<tbody>
							<?php while($row = mysqli_fetch_assoc($res)){?>
								<tr>
									<td class="text-center"><?php echo $row["idmodulo"]; ?></td>
									<td class="text-center"><?php echo $row["nomemodulo"]; ?></td>
									<td class="text-center"><?php echo $row["descricao"]; ?></td>
									<td class="text-center"><?php echo $row["categoria"]; ?></td>
									<td class="text-center"><?php echo date('d/m/Y H:i:s',strtotime($row["created"])); ?></td>
									<td class="text-center">								
										<a href="#">
											<span class="glyphicon glyphicon-eye-open text-primary" aria-hidden="true"></span>
										</a>
										<a href="#">
											<span class="glyphicon glyphicon-pencil text-warning" aria-hidden="true"></span>
										</a>
										<a href="#">
											<span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span>
										</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			
			<?php
				//Verificar pagina anterior e posterior
				$pagina_anterior = $pagina - 1;
				$pagina_posterior = $pagina + 1;
			?>
			<nav class="text-center">
				<ul class="pagination">
					<li>
						<?php 
							if($pagina_anterior != 0){
								?><a href="administrativo.php?link=50&pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a><?php
							}else{
								?><span aria-hidden="true">&laquo;</span><?php
							}
						?>
					</li>
					<?php
						//Apresentar a paginação
						for($i = 1; $i < $num_pagina + 1; $i++){
							?>
								<li><a href="administrativo.php?link=50&pagina=<?php echo $i; ?>">
									<?php echo $i; ?>
								</a></li>
							<?php
						}
					?>
					<li>
						<?php 
							if($pagina_posterior <= $num_pagina){
								?><a href="administrativo.php?link=50&pagina=<?php echo $pagina_posterior; ?>" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a><?php
							}else{
								?><span aria-hidden="true">&raquo;</span><?php
							}
						?>
					</li>
				</ul>
			</nav>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>
 
