<?php
session_start();
include_once("funcaoBanco.php");
$idusuario = $_GET['idusuario'];
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar seu login!");
}
?>
<!DOCTYPE html>
<html>
<meta charset="utf-8">
<title>Editar Usuário</title>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 500px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 150px;

        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }
    </style>
</head>

<body>

</body>
<center>
    <div>
        <form action="bdusuario.php" method="post">
            <img src="logo-assinatura.png">
            <input type="hidden" name="idusuario" value=<?php echo $idusuario; ?>>
            <p><br>Nome *: <input type="text" name="nome"></p>
            <p>E-mail *: <input type="email" name="email"> </p>
            <p>Senha *: <input type="password" name="senha"> </p>
            <p>Confirmar Senha *: <input type="password" name="confirmarSenha"> </p>
            <p>Campos com * obrigatórios</p>
            <p>Selecione a Permissão:</p>
            <p><select name="selectPermissao" class="basic">
                    <option value="1">Total</option>
                    <option value="2">Edição</option>
                    <option value="3">Visualização</option>
                </select></p>
           <p> <button type="submit">Atualizar Usuário</button></p>
        </form>
        </form>
        <form action="delUsuario.php" method="post">
            <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>">
            <p><input type="submit" value="Deletar Usuário"></p>
            <p><a href="menu.php">Voltar ao Menu</a></p>
        </form>

    </div>
</center>

</html>