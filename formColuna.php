<?php
session_start();
if (!isset($_SESSION['nome'])) {
    header("location: index.php?msg=Favor realizar o Login!");
}
?>
<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cadastro de Campo</title>
    <meta charset="utf-8">
    <style>
        body {
            background-image: url("loja2.png");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        div {
            background-color: whitesmoke;
            border: 20px black;
            padding: 15px;
            border-radius: 10px;
            width: 500px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 200px;
        }

        p {
            color: black;
        }

        button {
            border: black;
            border-radius: 10px;
        }

        input {
            border-radius: 10px;
        }
    </style>
</head>

<body>
    <center>
        <div>
            <form action="cadColuna.php" method="post">
                <img src="logo-assinatura.png">
                <p><br>Nome do Campo *: <input type="text" name="nomecampo"></p>
                <p>Selecione o Tipo de Dado: <select name="tipoDado" class="basic">
                        <option value="text">TEXT</option>
                        <option value="int">INT</option>
                        <option value="varchar">VARCHAR</option>
                        <option value="boolean">BOOLEAN</option>
                    </select> </p>
                    <p>Tamanho do Campo *: <input type="text" name="tamanho"></p>
                    </p> <input type="checkbox" name="null">Null</p>
                <p>Campos com * obrigatórios</p>
                <button type="submit">Cadastrar</button>
                <p><a href="menu.php">Voltar ao Menu</a></p>
            </form>
        </div>
    </center>
</body>

</html>